# README #

This is very simply 3D printer host controller app in 16 lines of code! Written on C++.
For compile, type g++ main.cpp -lfltk -o 3dhost
The fltk-devel package is required.
![Screenshot_20161130_012754.png](https://bitbucket.org/repo/zEqer6/images/2529658891-Screenshot_20161130_012754.png)