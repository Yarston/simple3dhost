#include <unistd.h>
#include <fcntl.h>
#include <stdint.h>

#include <FL/Fl_Button.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Counter.H>
#include <FL/Fl_Widget.H>
#include <FL/Fl.H>

const char* data[] = {
    "X move", "G90\nG1 X%.1f F3000\n",
    "Y move", "G90\nG1 Y%.1f F3000\n",
    "Z move", "G90\nG1 Z%.1f F3000\n",
    "Extrude", "G90\nG1 E%.1f F3000\n",
    "End", "M303 E0 S%.1f\n",
    "Bed", "M303 E1 S%.1f\n",
    "X home", "G28 X0\n",
    "Y home", "G28 Y0\n",
    "Z home", "G28 Z0\n",
    "Beep", "M300 S3000 P100\n",
    "End Off", "M303 E0 S0\n",
    "Bed Off", "M303 E1 S0\n"
};
Fl_Counter* counters[6];
int buff[10];
int main() {
    Fl_Window window(310, 310, "Управление");
    window.begin();
    for (int i = 0; i < 12; i++) {
        Fl_Widget* widget = i < 6 ? (Fl_Widget*) (counters[i] = new Fl_Counter(10, 10 + i * 50, 200, 30, data[i * 2])) : (Fl_Widget*) new Fl_Button(220, 10 + (i - 6) * 50, 80, 30, data[i * 2]);
        if (i < 6) ((Fl_Counter*) widget)->step(1, 10);
        widget->user_data((int*) i);
        widget->callback([](Fl_Widget* w, void* inner) {if (write(buff[9] == 0 ? (buff[9] = open("/dev/ttyUSB0", O_RDWR | O_NONBLOCK | O_NDELAY)) : buff[9], (char*) buff, sprintf((char*) buff, data[1 + 2 * reinterpret_cast<uintptr_t> (inner)], ((Fl_Counter*) w)->value())) > 0 && reinterpret_cast<uintptr_t> (inner) < 9 && reinterpret_cast<uintptr_t> (inner) >= 6) counters[reinterpret_cast<uintptr_t> (inner) - 6]->value(0);});
    }
    window.end();
    window.show();
    return Fl::run();
}

